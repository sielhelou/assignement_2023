package ma.octo.assignement.exceptions;

public class MontantDepotInvalideException extends Exception {
   
  private static final long serialVersionUID = 1L;

  public MontantDepotInvalideException() {
  }
  public MontantDepotInvalideException(String message) {
        super(message);
  }
}


