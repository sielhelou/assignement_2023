package ma.octo.assignement.web.controllers;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MontantDepotInvalideException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.service.AuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;



@RestController(value = "/Deposit")
class DepositController {

    private static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(DepositController.class);
    
    private final CompteRepository compteRepository;
    private final DepositRepository depositRepository;
    private final AuditService auditService;
    
    @Autowired
    DepositController(CompteRepository compteRepository, DepositRepository depositRepository, AuditService auditService) {
        this.compteRepository = compteRepository;
        this.depositRepository = depositRepository;
        this.auditService = auditService;
    }

@PostMapping("/deposit")
@ResponseStatus(HttpStatus.CREATED)
public void deposit(@RequestBody DepositDto depositDto)
        throws CompteNonExistantException, MontantDepotInvalideException {

    Compte compte = compteRepository.findByNrCompte(depositDto.getNrCompte());

    if (compte == null) {
        LOGGER.error("Compte Non existant");
        throw new CompteNonExistantException("Compte Non existant");
    }

    BigDecimal montant = depositDto.getMontant();
    if (montant == null || montant.intValue() < 1 || montant.intValue() > MONTANT_MAXIMAL) {
        LOGGER.error("Montant de dépôt non valide");
        throw new MontantDepotInvalideException("Montant de dépôt non valide");
    }

    compte.setSolde(compte.getSolde().add(montant));
    
    compteRepository.save(compte);

    Deposit deposit = new Deposit();
    deposit.setDateExecution(depositDto.getDate());
    deposit.setCompteBeneficiaire(compte);
    deposit.setMontant(montant);
    depositRepository.save(deposit);

    auditService.auditDeposit("Dépôt de " + montant + " sur le compte " + depositDto.getNrCompte());
}

}