package ma.octo.assignement.web.controllers;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.AuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController(value = "/transfers")
class TransferController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    private final CompteRepository compteRepository;
    private final TransferRepository transferRepository;
    private final AuditService auditService;
    private final UtilisateurRepository utilisateurRepository;

    @Autowired
    TransferController(CompteRepository compteRepository, TransferRepository transferRepository, AuditService auditService, UtilisateurRepository utilisateurRepository) {
        this.compteRepository = compteRepository;
        this.transferRepository = transferRepository;
        this.auditService = auditService;
        this.utilisateurRepository = utilisateurRepository;
    }
    

    @GetMapping("listeDesTransferts")
    public List<Transfer> loadAll() {
        LOGGER.info("Listing all transfers");
        List<Transfer> allTransfers = transferRepository.findAll();

        if (allTransfers.isEmpty()) {
            return null;
        } else {
            return allTransfers;
        }
    }

    @GetMapping("listeDesComptes")
    public List<Compte> loadAllCompte() {
        List<Compte> allComptes = compteRepository.findAll();
        if (allComptes.isEmpty()) {
            return null;
        } else {
         return allComptes;
        }
    }
    
    @GetMapping("listeDesUtilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        List<Utilisateur> allUtilisateurs = utilisateurRepository.findAll();
        if (allUtilisateurs.isEmpty()) {
          return null;
        } else {
         return allUtilisateurs;
        }
    }


    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        Compte Emetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte Beneficiaire = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        if (Emetteur == null) {
            LOGGER.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (Beneficiaire == null) {
            LOGGER.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        BigDecimal montant = transferDto.getMontant();

        if (montant == null) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (montant.intValue() < 10) {
            LOGGER.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (montant.intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getMotif().length() < 0) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (Emetteur.getSolde().compareTo(transferDto.getMontant()) < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
            }

        Emetteur.setSolde(Emetteur.getSolde().subtract(transferDto.getMontant()));
        compteRepository.save(Emetteur);

        Beneficiaire.setSolde(new BigDecimal(Beneficiaire.getSolde().intValue() + transferDto.getMontant().intValue()));
        compteRepository.save(Beneficiaire);

        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setCompteBeneficiaire(Beneficiaire);
        transfer.setCompteEmetteur(Emetteur);
        transfer.setMontantTransfer(transferDto.getMontant());

        transferRepository.save(transfer);

        auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                        .toString());
    }

   
}
