package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;



public class DepositDto {
    private String nrCompte;
    private BigDecimal montant;
    private Date date;

    public String getNrCompte() {
        return nrCompte;
    }

    public void setNrCompte(String nrCompte) {
        this.nrCompte = nrCompte;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDate() {
        return date;
      }
    
    public void setDate(Date date) {
        this.date = date;
    }
}
